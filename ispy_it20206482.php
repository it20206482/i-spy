class ispy {
	var $log_file = "logs";
	var $telegram_notice = true;
	var $telegram_api = "[TELEGRAM_BOT_API]";
	var $telegram_chatid = "[TELEGRAM_CHATID]";
	var $protect_unset_global = true;
	var $protect_range_ip_deny = false;
	var $protect_range_ip_spam = false;
	var $protect_url = true;
	var $protect_request_server = true;
	var $protect_santy = true;
	var $protect_bots = true;
	var $protect_request_method = true;
	var $protect_dos = true;
	var $protect_union_sql = true;
	var $protect_click_attack = true;
	var $protect_xss = true;
	var $protect_cookies = true;
	var $protect_post = true;
	var $protect_get = true;
	var $protect_upload = true;
	var $protect_upload_maxsize = 25; // Max file size in mb for the web site dev can edit the value
	var $protect_upload_sanitise_fn = true;



	function sendNotification($chatid, $text) {
		return file_get_contents("https://api.telegram.org/bot".$this->telegram_api."/sendMessage?chat_id=".$chatid."&text=".$text);
	}

	private function unset_globals() {
		if ( ini_get('register_globals') ) {
			$allow = array('_ENV' => 1, '_GET' => 1, '_POST' => 1, '_COOKIE' => 1, '_FILES' => 1, '_SERVER' => 1, '_REQUEST' => 1, 'GLOBALS' => 1);
			foreach ($GLOBALS as $key => $value) {
				if ( ! isset( $allow[$key] ) ) unset( $GLOBALS[$key] );
			}
		}
	}

	private function get_env($st_var) {
		global $HTTP_SERVER_VARS;
		if(isset($_SERVER[$st_var])) {
			return strip_tags( $_SERVER[$st_var] );
		} elseif(isset($_ENV[$st_var])) {
			return strip_tags( $_ENV[$st_var] );
		} elseif(isset($HTTP_SERVER_VARS[$st_var])) {
			return strip_tags( $HTTP_SERVER_VARS[$st_var] );
		} elseif(getenv($st_var)) {
			return strip_tags(getenv($st_var));
		} elseif(function_exists('apache_getenv') && apache_getenv($st_var, true)) {
			return strip_tags(apache_getenv($st_var, true));
		}
		return '';
	}

	private function get_referer() {
		if( $this->get_env('HTTP_REFERER') )
			return $this->get_env('HTTP_REFERER');
		return 'no referer';
	}

	private function get_ip() {
		if ($this->get_env('HTTP_X_FORWARDED_FOR')) {
			return $this->get_env('HTTP_X_FORWARDED_FOR');
		} elseif ($this->get_env('HTTP_CLIENT_IP')) {
			return $this->get_env('HTTP_CLIENT_IP');
		} else {
			return $this->get_env('REMOTE_ADDR');
		}
	}

	private function get_user_agent() {
		if($this->get_env('HTTP_USER_AGENT'))
			return $this->get_env('HTTP_USER_AGENT');
		return 'none';
	}

	private function get_query_string() {
		if($this->get_env('QUERY_STRING'))
			return str_replace('%09', '%20', $this->get_env('QUERY_STRING'));
		return '';
	}

	private function get_request_method() {
		if($this->get_env('REQUEST_METHOD'))
			return $this->get_env('REQUEST_METHOD');
		return 'none';
	}

	private function get_host() {
		if ($this->protect_server_ovh === true OR $this->protect_server_kimsufi === true OR $this->protect_server_dedibox === true OR $this->protect_server_digicube === true ) {
			if (@empty($_SESSION['$this->gethostbyaddr'])) {
				return $_SESSION['$this->gethostbyaddr'] = @gethostbyaddr($this->get_ip());
			} else {
				return strip_tags($_SESSION['$this->gethostbyaddr']);
			}
		}
	}